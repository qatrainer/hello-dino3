package com.qa.HelloDino;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloDinoController {

    @RequestMapping("/")
    String helloDino() {
        return "<h1>Hello Dino!</h1>";

    }

}